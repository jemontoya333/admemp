# AdmEmp
# admempleados
## Como se inicia el API
Se debe correr el Main de Spring Boot.
Este indica que se crea y levanta el servicio en el localhost:8080/api/empleados

## Herramientas usadas para trabajar 
Postman 
Configuración de parametros requeridos para un post:
localhost:8080/api/empleados
{
    "primerApellido": "JOSE",
    "segundoApellido": "JULIO",
    "primerNombre": "PEPE",
    "otrosNombres": "AMARELO",
    "pais": "COLOMBIA",
    "tipoId": "CC",
    "id": "121212121213",
    "fechaIngreso": "01/02/2019",
    "area": "Administrativo",
    "estado": "Inactivo"
}

Configuración de parametros requeridos para un get:
localhost:8080/api/empleados/{id}

## Como consultar todos los empleados
En la herramienta postman se escribe con get localhost:8080/api/empleados 

## Como consultar 1 empleado
En la herramienta postman se escribe con get localhost:8080/api/empleados/{id}

## Como ingresar un nuevo empleado
En la herramienta postman se escribe con get localhost:8080/api/empleados/
y se agrega en el body con formato json los parametros arriba suministrados o los de su preferencia.

## Base de datos usada:
### MySQL
