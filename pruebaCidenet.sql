CREATE SCHEMA `dbempleados` ;


CREATE TABLE `empleado` (
  `primerApellido` varchar(20) NOT NULL,
  `segundoApellido` varchar(20) NOT NULL,
  `primerNombre` varchar(20) NOT NULL,
  `otrosNombres` varchar(50) DEFAULT NULL,
  `pais` varchar(20) NOT NULL,
  `tipoid` varchar(30) NOT NULL,
  `id` varchar(20) NOT NULL,
  `correo` varchar(300) NOT NULL,
  `fechaIngreso` date NOT NULL,
  `area` varchar(45) NOT NULL,
  `estado` varchar(20) NOT NULL DEFAULT 'Activo',
  `fechaRegistro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci